export function configureFakeBackend() {
    let users = [
        { id:1, username: 'Alex', password: '48690', status: 'trader'},
        { id:2, username: 'Nadia', password: '52594', status: 'security officer'}
    ];

    let fakeData = [
        {},
        {},
        {},
        {}
    ];
    let realFetch = window.fetch;
    window.fetch = function (url, opts) {
        return new Promise((resolve, reject) => {
            // wrap in timeout to simulate server api call
            setTimeout(() => {

                console.log('I tried');
                // if (url.endsWith('/database-status'))
                // {
                //     const responseJson = { databaseStatus: 'up' };
                //     console.log('responseJson: ', responseJson);
                //     // resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(responseJson))})
                //     resolve({ ok: true, text: JSON.stringify(responseJson)});

                //     return;
                // }

                // if(url.endsWith('/average-price')) {
                //     const responseJson = [
                //         { instrumentName: 'Astronomica',
                //           buyAverage: 24,
                //           sellAverage: 46
                //          },
                //          {
                //             instrumentName: 'Celestial',
                //             buyAverage: 34,
                //             sellAverage: 112
                //          },
                //          {
                //             instrumentName: 'Jupiter',
                //             buyAverage: 180,
                //             sellAverage: 30
                //          }
                //     ];
                //     console.log('responseJson: ', responseJson);
                //     // resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(responseJson))})
                //     resolve({ ok: true, text: JSON.stringify(responseJson)});

                //     return;
                // }

                // if (url.endsWith('/ending-position')) {
                //     const responseJson =
                //         {
                //             endingPosition: 58
                //         };
                //     console.log('responseJson: ', responseJson);
                //     // resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(responseJson))})
                //     resolve({ ok: true, text: JSON.stringify(responseJson)});
                //     return;
                // }

                // if (url.endsWith('/realized-profit')) {
                //     const responseJson = [
                //         { instrumentName: 'Astronomica',
                //           realizedProfit: 98
                //          },
                //          {
                //             instrumentName: 'Celestial',
                //             realizedProfit: 38
                //          },
                //          {
                //             instrumentName: 'Jupiter',
                //             realizedProfit: -46
                //          }
                //     ];
                //     console.log('responseJson: ', responseJson);
                //     // resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(responseJson))})
                //     resolve({ ok: true, text: JSON.stringify(responseJson)});
                //     return;
                // }


                // if (url.endsWith('/effective-profit')) {
                //     const responseJson = [
                //         { instrumentName: 'Astronomica',
                //           effectiveProfit: 98
                //          },
                //          {
                //             instrumentName: 'Celestial',
                //             effectiveProfit: -65
                //          },
                //          {
                //             instrumentName: 'Jupiter',
                //             effectiveProfit: 76
                //          }
                //     ];
                //     console.log('responseJson: ', responseJson);
                //     // resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(responseJson))})
                //     resolve({ ok: true, text: JSON.stringify(responseJson)});
                //     return;
                // }


                if (url.endsWith('/login') && opts.method === 'POST') {
                    // get parameters from post request
                    let params = JSON.parse(opts.body);
                    console.log('Params:', params);
                    // find if any user matches login credentials
                    let filteredUsers = users.filter(user => {
                        return user.username === params.username && user.password == params.passwordHash;
                    });
                    console.log(filteredUsers);
                    if (filteredUsers.length) {
                        // if login details are valid return user details
                        let user = filteredUsers[0];
                        let responseJson = {
                            role: user.status,
                            username: user.username,
                        };
                        resolve({ ok: true, text: JSON.stringify(responseJson) });
                    } else {
                        // else return error
                        reject('Username or password is incorrect');
                    }

                    return;
                }

                // pass through any requests not handled above
                realFetch(url, opts).then(response => resolve(response));

            }, 500);
        });
    }
}