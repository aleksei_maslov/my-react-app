import React from 'react'
import { Route, Redirect } from 'react-router-dom'

// Renders a route only if the user is logged in, otherwise redirects the user to the /login page
const RouteIfIsLoggedIn = ({ isLoggedIn, user, showDeals, handleLoggedOut, component: Component, ...rest }) => (
    <Route {...rest} render={props => {
        const newProps = { ...props, isLoggedIn, user, showDeals, handleLoggedOut }
        return (isLoggedIn ?
            <Component {...newProps} /> :
            <Redirect to={{ pathname: '/login' }} />)
    }} />
);

export default RouteIfIsLoggedIn;