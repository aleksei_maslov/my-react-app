import React, { useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
import { Button, Form, Row, Col } from 'react-bootstrap';



const TraderPage = ({ user }) => {
    console.log(user);

    const [cpty, setCpty] = useState('');
    const [someState, setSomeState] = useState(true);
    const [someState2, setSomeState2] = useState(true);
    const [someState3, setSomeState3] = useState(true);
    const [averagePricesData, setAveragePricesData] = useState({
        labels:[],
        datasets: [
          {
            label: 'Buy average',
            backgroundColor: 'rgba(0,0,255,0.2)',
            borderColor: 'rgba(0,99,255,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(0,99,255,0.4)',
            hoverBorderColor: 'rgba(0,99,255,1)',
            data: []
          },
          {
            label: 'Sell average',
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: []
          }
        ]
      } );
    const [endingPosition, setEndingPosition] = useState(0);
    const [realizedProfitData, setRealizedProfitData] = useState({
        labels:[],
        datasets: [
          {
            label: 'Realized profit/loss',
            backgroundColor: 'rgba(0,0,255,0.2)',
            borderColor: 'rgba(0,99,255,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(0,99,255,0.4)',
            hoverBorderColor: 'rgba(0,99,255,1)',
            data: []
          }
        ]
      } );
    const [effectiveProfitData, setEffectiveProfitData] = useState({
        labels:[],
        datasets: [
          {
            label: 'Effective profit/loss',
            backgroundColor: 'rgba(0,0,255,0.2)',
            borderColor: 'rgba(0,99,255,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(0,99,255,0.4)',
            hoverBorderColor: 'rgba(0,99,255,1)',
            data: []
          }
        ]
      } );
    const [fromDate, setFromDate] = useState({});
    const [toDate, setToDate] = useState({})
    const [chartReference, setChartReference] = useState('');

    useEffect(() => {
        const fetchEndingPosition = async () => {
            try {
                const response = await fetch(process.env.REACT_APP_API_URL + '/ending-position');
                const data = JSON.parse(response.text);
                console.log('response: ', response);
                console.log('data: ', data);
                setEndingPosition(data.endingPosition);
            } catch (error) {
                console.log(error);
            }
        };

        const fetchEffectiveProfit = async () => {
            try {
                console.log('effective profit url: ', process.env.REACT_APP_API_URL + '/effective-profit');
                const response = await fetch(process.env.REACT_APP_API_URL + '/effective-profit');
                const data = await response.json();
                console.log('effective response data: ', data);
                // const data = JSON.parse(response.text); => const data = await response.json();
                const labels = [];
                const effectiveProfits = [];
                for (let i = 0; i < data.length; i++) {
                    const obj = data[i];
                    labels.push(obj.instrumentName);
                    effectiveProfits.push(obj.effectiveProfit);
                }

                const effectiveProfitDataCopy = effectiveProfitData;

                effectiveProfitDataCopy.labels = labels;
                effectiveProfitDataCopy.datasets[0].data = effectiveProfits;

                console.log(effectiveProfitDataCopy);

                setEffectiveProfitData(effectiveProfitDataCopy);

                setSomeState2(!someState2);
            } catch (error) {
                console.log(error);
            }
        };

        const fetchRealizedProfit = async () => {
            try {
                const response = await fetch(process.env.REACT_APP_API_URL + '/realized-profit');
                console.log('RealizedProfitresponse: ', response);
                const data = await response.json();
                console.log('RealizedProfitdata: ', data);                
                const labels = [];
                const realizedProfits = [];
                for (let i = 0; i < data.length; i++) {
                    const obj = data[i];
                    labels.push(obj.instrumentName);
                    realizedProfits.push(obj.realizedProfit);
                }

                const realizedProfitDataCopy = realizedProfitData;

                realizedProfitDataCopy.labels = labels;
                realizedProfitDataCopy.datasets[0].data = realizedProfits;

                console.log(realizedProfitDataCopy);

                setRealizedProfitData(realizedProfitDataCopy);

                setSomeState3(!someState3);
            } catch (error) {
                console.log(error);
            }
        };
        const fetchAveragePrices = async () => {
            try {
                const response = await fetch(process.env.REACT_APP_API_URL + '/average-prices');
                console.log('response: ', response);
                const data = await response.json();
                console.log('average respoces data: ', data);
                console.log('data: ', data[0]);
                const labels = [];
                const averageBuys = [];
                const averageSells = [];
                for (let i = 0; i < data.length; i++) {
                    const obj = data[i];
                    labels.push(obj.instrumentName);
                    averageBuys.push(obj.buyAverage);
                    averageSells.push(obj.sellAverage);
                }

                const averagePricesDataStateCopy = averagePricesData;

                averagePricesDataStateCopy.labels = labels;
                averagePricesDataStateCopy.datasets[0].data = averageBuys;
                averagePricesDataStateCopy.datasets[1].data = averageSells;

                console.log(averagePricesDataStateCopy);

                setAveragePricesData(averagePricesDataStateCopy);

                setSomeState(!someState);
            } catch (error) {
                console.log(error);
            }
        };

        fetchAveragePrices();
        fetchEndingPosition();
        fetchRealizedProfit();
        fetchEffectiveProfit();
    }, []);


    const data = {
        labels: ["Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
        "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic"],
        datasets: [
          {
            label: 'My First dataset',
            backgroundColor: 'rgba(0,0,255,0.2)',
            borderColor: 'rgba(0,99,255,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(0,99,255,0.4)',
            hoverBorderColor: 'rgba(0,99,255,1)',
            data: [65, 59, 80, 81, 56, 55, 40]
          },
          {
            label: 'My SND dataset',
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: [-65, -59,-80, -81, -56, -55, -40]
          }
        ]
      };

    return (
        <div>
            {console.log('updated with averagePricesData: ', averagePricesData)}
            <div className='graph_form'>
                {/* <Form>
                    <Form.Group as={Row} controlId="formPlaintextEmail">
                        <Form.Label column sm="1">
                            From:
                        </Form.Label>
                        <Col sm="2">
                            <Form.Control type="date"/>
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row} controlId="formPlaintextPassword">
                        <Form.Label column sm="1">
                            To:
                        </Form.Label>
                        <Col sm="2">
                            <Form.Control type="date" />
                        </Col>
                    </Form.Group>
                    <Button variant="primary">Update</Button>
                </Form> */}
                <div style={{border: 'solid lightgrey 1pt', marginTop: '2%'}}>

                    <Bar
                        ref={(reference) => setChartReference(reference)}
                        data={averagePricesData}
                        width={100}
                        height={500}
                        options={{
                            maintainAspectRatio: false
                        }}
                    />
                </div>
                <div className='trader_data'>Ending Position: {endingPosition}</div>
            
                <div className='trader_data'>Your realized profit: </div>
                <div style={{border: 'solid lightgrey 1pt', marginTop: '2%'}}>

                    <Bar
                        ref={(reference) => setChartReference(reference)}
                        data={realizedProfitData}
                        width={100}
                        height={500}
                        options={{
                            maintainAspectRatio: false
                        }}
                    />
                </div>
                <div className='trader_data'>Your effective profit: </div>
                <div style={{border: 'solid lightgrey 1pt', marginTop: '2%'}}>

                    <Bar
                        ref={(reference) => setChartReference(reference)}
                        data={effectiveProfitData}
                        width={100}
                        height={500}
                        options={{
                            maintainAspectRatio: false
                        }}
                    />
                </div>

            </div>
        </div>
    );
};

export default TraderPage;