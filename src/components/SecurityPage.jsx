import React, { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';


const SecurityPage = ({ user }) => {
    const [databaseStatus, setDatabaseStatus] = useState('as');

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(process.env.REACT_APP_API_URL + '/database-status');
                const data = JSON.parse(response.text);
                console.log('response: ', response);
                console.log('data: ', data);
                console.log('data.databaseStatus: ', data.databaseStatus);
                setDatabaseStatus(data.databaseStatus);
            } catch (error) {
                console.log(error);
            }
        };

        fetchData();
    }, []);

    const databaseIsUp = () => databaseStatus === 'up';
    const databaseIsDown = () => databaseStatus === 'down';

    return (
        <div>
        {console.log(databaseStatus)}
            <div id='status_block'>
                <p>Database status is: 
                {databaseIsDown() && <span id='status-stop'> Stopped</span>}
                {databaseIsUp() && <span id='status-run'> Running</span>} 
                {!databaseIsDown() && !databaseIsUp() && <span id='status-unknown'> Unknown</span>}</p>
            </div> 
        </div>
    );
};

export default SecurityPage;