import React, { Component } from "react";
import { Table, Form, Row, Col, Button, Pagination } from "react-bootstrap";
import { Line } from "react-chartjs-2";

class TableContentPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fakeState: false,
            notes: [],
            notesPerPage: 10,
            pagesAmount: 1,
            currentPage: 1
        };

        this.eventSource = new EventSource(process.env.REACT_APP_API_URL + '/deals/stream');
        this.addNote = this.addNote.bind(this);
    }

    // SOMEWHERE HERE IS A BUG. FIX PLS

    addNote({ instrumentName, cpty, price, type, quantity, time }) {
        this.setState(prevState => ({
            notes: [{ instrumentName, cpty, price, type, quantity, time }, ...prevState.notes],
            pagesAmount: Math.ceil((prevState.notes.length + 1) / prevState.notesPerPage)
        }));
    };

    componentDidMount() {
        this.subscribeToEventStream();
    }

    updateNotesState(data) {
        this.addNote(data);
    }

    subscribeToEventStream() {
        this.eventSource.onmessage = event => {
            console.log(event);
            console.log(event.data);
            this.updateNotesState(JSON.parse(event.data));
        };
    }

    renderPagination() {
        let pageItems = [];
        for (let number = 1; number <= this.state.pagesAmount && number <= 10; number++) {
            pageItems.push(
                <Pagination.Item key={number} onClick={event => { this.setState({ currentPage: event.target.textContent }); 
                    console.log(this.state); console.log(event.target.textContent); }}>{number}</Pagination.Item>
            );
        }
        return (
            <div style={{float: 'right'}}><Pagination bsSize="small">{pageItems}</Pagination></div>
        );
    }

    render() {
        console.log('Render called with notes: ', this.state);
        return (
            <div className='mytable'>
                {/* <Form>
                    <Form.Group as={Row} controlId="formPlaintextEmail">
                        <Form.Label column sm="1">
                            From:
                        </Form.Label>
                        <Col sm="2">
                            <Form.Control type="date" />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row} controlId="formPlaintextPassword">
                        <Form.Label column sm="1">
                            To:
                        </Form.Label>
                        <Col sm="2">
                            <Form.Control type="date" />
                        </Col>
                    </Form.Group>
                    <Button variant="primary">Update</Button>
                </Form> */}

                {this.renderPagination()}

                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Instrument</th>
                            <th>Cpty</th>
                            <th>Price</th>
                            <th>Type</th>
                            <th>Quantity</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.notes.map((note, index) => {
                            const from = (this.state.currentPage - 1) * this.state.notesPerPage;
                            const to = this.state.currentPage * this.state.notesPerPage;
                            if (index >= from && index < to) {
                                return (
                                    <tr key={index}>
                                        <td>{note.instrumentName}</td>
                                        <td>{note.cpty}</td>
                                        <td>{parseFloat(Math.round(note.price * 100) / 100).toFixed(2)}</td>
                                        <td>{note.type}</td>
                                        <td>{note.quantity}</td>
                                        <td>{note.time}</td>
                                    </tr>
                                );
                            } else {
                                return (<div></div>);
                            }
                        })}
                    </tbody>
                </Table>
                {/* <div style={{border: 'solid lightgrey 1pt', marginTop: '2%'}}>
                    <Line
                        ref={(reference) => setChartReference(reference)}
                        data={averagePricesData}
                        width={100}
                        height={500}
                        options={{
                            maintainAspectRatio: false
                        }}
                    />
                </div> */}
            </div>
        );
    }
}

export default TableContentPage;