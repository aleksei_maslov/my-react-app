import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Bar } from 'react-chartjs-2';
import { ButtonGroup } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { userService } from '../helpers/userService';

const SeniorTraderPage = () => {
    const [user, setUser] = useState({});
    const [users, setUsers] = useState([]);
    const [data, setData] = useState({});

    useEffect(() => {
        setUser(JSON.parse(localStorage.getItem('user')));
        setUsers({ loading: true });

        const fetchData = async () => {
            try {
                const users = await userService.getAll();
                setUsers(users);
            } catch (e) {
                console.log(e)
            }
        };

        fetchData();
    }, []);

    const data1 = {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [
          {
            label: 'My First dataset',
            backgroundColor: 'rgba(0,0,255,0.2)',
            borderColor: 'rgba(0,99,255,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(0,99,255,0.4)',
            hoverBorderColor: 'rgba(0,99,255,1)',
            data: [11, 12, 80, 98, 0, 55, 40]
          }
        ]
    };

    const data2 = {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [
          {
            label: 'My First dataset',
            backgroundColor: 'rgba(0,0,255,0.2)',
            borderColor: 'rgba(0,99,255,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(0,99,255,0.4)',
            hoverBorderColor: 'rgba(0,99,255,1)',
            data: [0, 32, 25, 81, 56, 11, 20]
          }
        ]
    };

    const data3 = {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [
          {
            label: 'My First dataset',
            backgroundColor: 'rgba(0,0,255,0.2)',
            borderColor: 'rgba(0,99,255,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(0,99,255,0.4)',
            hoverBorderColor: 'rgba(0,99,255,1)',
            data: [65, 59, 80, 81, 56, 55, 40]
          }
        ]
    };

    const data4 = {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [
          {
            label: 'My First dataset',
            backgroundColor: 'rgba(0,0,255,0.2)',
            borderColor: 'rgba(0,99,255,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(0,99,255,0.4)',
            hoverBorderColor: 'rgba(0,99,255,1)',
            data: [23, 159, 11, 87, 15, 35, 10]
          }
        ]
    };

    const handle1 = () => {
        console.log('aaaa');
        setData(data1);
    }

    const handle2 = () => {
        console.log('bbb');
        setData(data2);
    }

    const handle3 = () => {
        console.log('cc');
        setData(data3);
    }

    const handle4 = () => {
        console.log('d');
        setData(data4);
    }
    
    return (
        <div class="container">
            <div class='head_block'>
                <Button  variant="primary" onClick={}>Main page</Button>
                <Button  variant="primary" onClick={}>Deals page</Button>
                <Button  variant="primary" onClick={}>Logout</Button>
            </div>
            <div>
                <div>Label</div>
                <form>
                    <div>
                        <label>From:</label>
                        <input type='date' value={fromDate} onChange={event => setFromDate(event.target.value)}></input>
                    </div>
                    <div>
                        <label>To:</label>
                        <input type='date' value={toDate} onChange={event => setToDate(event.target.value)}></input>
                    </div>
                    <Button variant="primary" onClick={}>Update</Button>
                </form>
            <ButtonGroup class="buttongroup">
                <Button  variant="primary" onClick={handle1}>Average price</Button>
                <Button  variant="primary" onClick={handle2}>Ending positions</Button>
                <Button  variant="primary" onClick={handle3}>Reliased profit/loss</Button>
                <Button  variant="primary" onClick={handle4}>Effective profit/loss</Button>
            </ButtonGroup>
            <Bar data={data}
                width={100}
                height={300}
                options={{
                maintainAspectRatio: false
            }}/>

                <div>Ending Position: {endingPosition}</div>
                <div>Realized profit: {realizedProfit}</div>
                <div>Effective profit: {effectiveProfit}</div>
            </div>
            <p>
                <Link to='/login'>Logout</Link>
            </p>
        </div>
    );
};

export default SeniorTraderPage;