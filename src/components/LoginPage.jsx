import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import {  Redirect } from 'react-router-dom'

const LoginPage = ({ handleLoggedIn, isLoggedIn }) => {
    // props.handleLoggedOut();

    const [username, setUsername] = useState('Alex');
    const [password, setPassword] = useState('123');
    const [errorText, setErrorText] = useState('');

    const validateForm = () => {
        return username.length > 0 && password.length > 0;
    }

    const hashFunction = str => {
        let hash = 0;

        if (str.length === 0) { 
            return hash;
        }

        for (let i = 0; i < str.length; i++) {
            const char = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + char;
            hash = hash & hash;
        }

        return hash;
    }

    const handleSubmit = event => {
        event.preventDefault();

        console.log('Log in submitted!');

        console.log('hashed password: ', hashFunction(password));

        const passwordHash = hashFunction(password);

        const fetchLogin = async () => {
            try {
                const response = await fetch(process.env.REACT_APP_API_URL + '/login', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({username , passwordHash})
                });
                if (response.ok) {
                    // const data = JSON.parse(response.text);
                    // const data = await response.json();
                    const data = JSON.parse(response.text);
                    console.log('response: ', response);
                    console.log('data: ', data);
                    // handleLoggedIn(data[0]);
                    handleLoggedIn(data);
                } else {
                    setErrorText('Invalid username or password.');
                    console.log('ERROR response: ', response);
                }
            } catch (error) {
                console.log(error);
            }
        };
        fetchLogin();
    };

    console.log('HASH of 123: ', hashFunction('123'));
    console.log('HASH of 3r1: ', hashFunction('3r1'));
    console.log('HASH of : ', password, ' is ', hashFunction(password));
    const handleUsernameChange = event => {
        setUsername(event.target.value);
    };

    const handlePasswordChange = event => {
        setPassword(event.target.value);
    };

    console.log('isLoggedIn:', isLoggedIn);

    return (
        <div>
            {isLoggedIn ? 

            <Redirect to='/'/> 
            
            :

            <div id='login_page'>

                <img 
                    id='logo_image' 
                    width="150" 
                    title="Deutsche Bank #PositiveImpact" 
                    alt="DB logo" 
                    src="https://www.dbresearch.de/res/images/rps/social/dblogo.png" 
                />

                <div id='login_form'>
                    <Form onSubmit={handleSubmit}>
                        <h1 id='login_title'>The floor is made of Java</h1>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Control 
                                type="text" 
                                placeholder="Username" 
                                name='username' 
                                value={username} 
                                onChange={handleUsernameChange}
                            />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Control 
                                type="password" 
                                placeholder="Password" 
                                name='password' 
                                value={password} 
                                onChange={handlePasswordChange}
                            />
                        </Form.Group>
                        {errorText &&
                        <div>
                            <p id='login_error_text'>{errorText}</p>
                        </div>
                        }
                        <div id='login_button'>
                            <Button 
                                className='w-50' 
                                variant="primary" 
                                type="submit"
                                disabled={!validateForm()}
                            >
                                Log in
                            </Button>
                        </div>
                    </Form>
                </div>
            </div>
            }
        </div>
    );
};

export default LoginPage;