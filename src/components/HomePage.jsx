import React from 'react';
import { Navbar, Button, Nav } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

import TraderPage from './TraderPage';
import TableContentPage from './TableContentPage';
import SecurityPage from './SecurityPage';

const HomePage = ({ isLoggedIn, user, showDeals, handleLoggedOut }) => {
    console.log(user);
    console.log(showDeals);
    console.log(user.role);
    const isTrader = () => user.role == 'trader';
    const shouldShowTraderPage = () => isTrader() && !showDeals;
    const shouldShowDealsPage = () => isTrader() && showDeals;
    const isSecurityOfficer = () => user.role == 'security officer';
    const shouldShowDatabaseStatusPage = () => isSecurityOfficer() && !isTrader();

    console.log('show trader page: ', shouldShowTraderPage());
    return (
        <div id='home_page'>
            <Navbar bg="primary" variant="dark">
                <Navbar.Brand >Group 24</Navbar.Brand>
                <LinkContainer to="/">
                    <div style={{margin: '10pt'}}><Button>Home page</Button></div>
                </LinkContainer>
                {isTrader() &&
                <LinkContainer to="/deals">
                <div style={{margin: '10pt'}}><Button className="m1">Deals data</Button></div>
                </LinkContainer>
                }
                <Nav className="mr-auto"></Nav>
                {/* <LinkContainer> */}
                    <Button onClick={event => handleLoggedOut()}>Logout</Button>
                {/* </LinkContainer> */}
            </Navbar>
            <div>{shouldShowTraderPage() && <TraderPage user={ user }/>}</div>
            <div>{shouldShowDealsPage() && <TableContentPage user={ user }/>}</div>
            <div>{shouldShowDatabaseStatusPage() && <SecurityPage user={ user }/>}</div>
        </div>
    );
}

export default HomePage;