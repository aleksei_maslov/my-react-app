import React, { useState } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import RouteIfIsLoggedIn from './components/RouteIfIsLoggedIn';
import LoginPage from './components/LoginPage';
import HomePage from './components/HomePage';

const App = (props) => {
  // const [isLoggedIn, setLoggedIn] = useState(false);
  // const [user, setUser] = useState({});
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [user, setUser] = useState({username:'Alex', role:'trader'});
  // const [user, setUser] = useState({name:'Nadia', role:'security officer'});

  const handleLoggedIn = (user) => {
    console.log('app handled logged in');
    if (user) {
      setUser(user);
      setLoggedIn(true);
    } else {
      setUser({});
      setLoggedIn(false);
    }
  };

  const handleLoggedOut = () => {
    console.log('handle logged out pressed.');
    setUser({});
    setLoggedIn(false);
  }

  return (
    <div>
      <Router>
        <Route 
          path='/login' 
          render={props => 
            <LoginPage 
              handleLoggedIn={handleLoggedIn}
              isLoggedIn={isLoggedIn}
              history={props.history} 
            />
          }
        />
        <RouteIfIsLoggedIn 
          exact path='/' 
          isLoggedIn={isLoggedIn} 
          user={user} 
          showDeals={false} 
          handleLoggedOut={handleLoggedOut} 
          component={HomePage}
        />
        <RouteIfIsLoggedIn 
          exact path='/deals' 
          isLoggedIn={isLoggedIn} 
          user={user} 
          showDeals={true} 
          handleLoggedOut={handleLoggedOut} 
          component={HomePage}
        />
      </Router>
    </div>
  );
};

export default App;